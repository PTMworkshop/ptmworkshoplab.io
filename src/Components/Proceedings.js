import React from 'react';
import panoptic_slides from "../Static/PANOPTIC-Update-WPTM-2024.pdf"
import linddun_slides from "../Static/LINDDUN-GO-Gamification.pdf"
import xcompass_slides from "../Static/xCOMPASS.pdf"
import wittig_slides from "../Static/Privacy-Threats-Online-Ads.pdf"
import sadeh_slides from "../Static/UsersFirst.pdf"
import liu_slides from "../Static/Liu-Biczok-paper.pdf"
import wuyts_slides from "../Static/WPTM24_keynote_growingpains.pdf"
import workshop_summary from "../Static/WPTM_Summary_2024.pdf"


export default function Proceedings(props) {

    return (

        <div className='base-page'>
            <h4 className='heading'>WPTM 2024</h4>
            <div class='paper'>
                <h5>
                <a href={workshop_summary} download="WPTM_Summary_2024.pdf">Workshop Summary</a>
                </h5>
                <h6>Samantha Katcher, MITRE</h6>
                <h6>Ben Ballard, MITRE</h6>
                <h6>Stuart Shapiro, MITRE</h6>
            </div>
        
            <h4 className='heading new-section'>Privacy Threat Model Updates</h4>
            <div class='paper'>
                <h5>
                <a href={panoptic_slides} download="PANOPTIC-Update-WPTM-2024.pdf">
                MITRE PANOPTIC™</a>
                </h5>
                <h6>Stuart Shapiro, MITRE</h6>
            </div>
            <div class='paper'>
                <h5>
                <a href={linddun_slides} download="LINDDUN-GO-Gamification.pdf">
                LINDDUN</a>
                </h5>
                <h6>Laurens Sion, LINDDUN</h6>
            </div>

            <div class='paper'>
                <h5>
                <a href={xcompass_slides} download="xCOMPASS.pdf">
                XCOMPASS</a>
                </h5>
                <h6> S.P.I.D.E.R. Team, Comcast </h6>
            </div>
     
            <h4 className='heading new-section'>Presentations</h4>
            <div class='paper'>
                <h5>Keynote</h5>
                <h5>
                <a href={wuyts_slides} download="WPTM24_keynote_growingpains.pdf">
                Growing Pains: The Teenage Years of Privacy Threat Modeling</a>
                </h5>
                <h6>Kim Wuyts, PwC Belgium</h6>
            </div>
            <div class='paper'>
                <h5>Presentation 1</h5>
                <h5>
                <a href={wittig_slides} download="Privacy-Threats-Online-Ads.pdf">
                Privacy Threats in Online Advertising</a>
                </h5>
                <h6>Maximilian Wittig</h6>
            </div>
            <div class='paper'>
                <h5>Presentation 2</h5>
                <h5>
                <a href={sadeh_slides} download="UsersFirst.pdf">
                “UsersFirst: A User-Centric Threat Modeling Framework for Notice & Choice”</a>
                </h5>
                <h6>Norman Sadeh</h6>
            </div>
            <div class='paper'>
                <h5>Presentation 3</h5>
                <h5>
                <a href={liu_slides} download="Liu-Biczok-paper.pdf">
                Eliciting and mitigating interdependent privacy threats with LINDDUN</a>
                </h5>
                <h6>Shuaishuai Liu</h6>
                </div>
    </div>
    )
   
}